using Ioasys.Application.Interfaces;
using Ioasys.Domain.Core.Notifications;
using Ioasys.Domain.Interfaces;
using Ioasys.WebApi.Controllers;
using MediatR;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Beblue.WebApi.Controllers
{
    [Route("api/[controller]/[action]")]
    public class EnterpriseController : BaseApiController
    {

        private readonly IEnterpriseAppService enterpriseAppService;

        public EnterpriseController(IEnterpriseAppService enterpriseAppService,
            INotificationHandler<DomainNotification> notifications,
            IMediatorHandler mediator)
            : base(notifications, mediator)
        {
            this.enterpriseAppService = enterpriseAppService;
        }

        /// <summary>
        /// Consultar a Listagem de Empresas
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            return Response(await enterpriseAppService.GetAll());
        }

        /// <summary>
        /// Consultar a empresa pelo seu identificador;   
        /// /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> GetEnterpriseById(int id)
        {
            return Response(await enterpriseAppService.GetById(id));

        }

        /// <summary>
        /// Filtro de Empresas por nome e tipo;   
        /// /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<IActionResult> FilterEnterprise(string name, int typeId )
        {
            return Response(await enterpriseAppService.FilterEnterprise( name, typeId));
        }
    }
}