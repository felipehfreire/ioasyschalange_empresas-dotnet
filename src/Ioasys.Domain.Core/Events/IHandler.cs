﻿using System.Threading.Tasks;

namespace Ioasys.Domain.Core.Events
{
    public interface IHandler<T> where T : Message
    {
        Task Handle(T message);
    }
}
