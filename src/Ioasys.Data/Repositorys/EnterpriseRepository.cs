﻿using Ioasys.Data.Context;
using Ioasys.Domain.Enterprises;
using Ioasys.Domain.Enterprises.Repository;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Ioasys.Data.Repositorys
{
    public class EnterpriseRepository : Repository<Enterprise>, IEnterpriseRepository
    {
        public EnterpriseRepository(IoasysContext context) : base(context)
        {
        }

        public async Task<List<Enterprise>> FilterEnterpriseAsync(string name, int typeId)
        {
            return await Db.Enterprises.Include(t => t.Enterprise_Type).Where(p => p.Enterprise_Name == name && p.TypeId == typeId).ToListAsync();
        }
    }
}
