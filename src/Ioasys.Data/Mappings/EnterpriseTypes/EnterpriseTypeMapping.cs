﻿using Ioasys.Data.Extensions;
using Ioasys.Domain.Enterprises;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ioasys.Data.Mappings.EnterpriseTypes
{
    class EnterpriseTypeMapping : EntityTypeConfiguration<EnterpriseType>
    {
        public override void Map(EntityTypeBuilder<EnterpriseType> builder)
        {
            builder.ToTable("EnterpriseTypes");
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Enterprise_Type_Name)
                .HasColumnType("varchar(200)")
                .IsRequired();

            // not map ValidationResult from fluent validation
            builder.Ignore(e => e.ValidationResult);
            //to igonre cascade mode validations
            builder.Ignore(e => e.CascadeMode);
        }
    }
}