﻿using Ioasys.Data.Extensions;
using Ioasys.Domain.Enterprises;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Ioasys.Data.Mappings.Enterprises
{
    class EnterpriseMapping : EntityTypeConfiguration<Enterprise>
    {
        public override void Map(EntityTypeBuilder<Enterprise> builder)
        {
            builder.ToTable("Enterprises");
            builder.HasKey(e => e.Id);
            builder.Property(e => e.Enterprise_Name)
                .HasColumnType("varchar(200)")
                .IsRequired();
            builder.Property(e => e.Description)
                .HasColumnType("varchar(500)")
                .IsRequired();


            builder.HasOne(e => e.Enterprise_Type)
              .WithMany(o => o.Enterprises)
              .HasForeignKey(e => e.TypeId);

            // not map ValidationResult from fluent validation
            builder.Ignore(e => e.ValidationResult);
            //to igonre cascade mode validations
            builder.Ignore(e => e.CascadeMode);
        }
    }
}
