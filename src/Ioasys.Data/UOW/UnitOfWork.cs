﻿using System.Threading.Tasks;
using Ioasys.Data.Context;
using Ioasys.Domain.Interfaces;
namespace Ioasys.Data.UOW
{
    public class UnitOfWork : IUnityOfWork
    {
        private readonly IoasysContext _context;

        public UnitOfWork(IoasysContext context)
        {
            _context = context;
        }

        public async Task<bool> Commit()
        {
            return await _context.SaveChangesAsync() > 0;
        }

        public void Dispose()
        {
            _context.Dispose();
        }

       
    }
}
