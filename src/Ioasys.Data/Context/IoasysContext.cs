﻿using Ioasys.Data.Extensions;
using Ioasys.Data.Mappings.Enterprises;
using Ioasys.Data.Mappings.EnterpriseTypes;
using Ioasys.Domain.Enterprises;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System.IO;

namespace Ioasys.Data.Context
{
    public class IoasysContext : DbContext
    {
        public DbSet<Enterprise> Enterprises { get; set; }
        

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.AddConfiguration(new EnterpriseMapping());
            modelBuilder.AddConfiguration(new EnterpriseTypeMapping());
            
            base.OnModelCreating(modelBuilder);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            var config = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();
            optionsBuilder.UseSqlServer(config.GetConnectionString("DefaultConnection"));
        }
    }
}
