﻿using System;
using System.Threading.Tasks;

namespace Ioasys.Domain.Interfaces
{
    public interface IUnityOfWork : IDisposable
    {
        Task<bool> Commit();
    }
}
