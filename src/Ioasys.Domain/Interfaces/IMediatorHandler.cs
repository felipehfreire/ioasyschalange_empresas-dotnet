﻿using Ioasys.Domain.Core.Commands;
using Ioasys.Domain.Core.Events;
using System.Threading.Tasks;

namespace Ioasys.Domain.Interfaces
{
    public interface IMediatorHandler
    {
        Task SendCommand<T>(T Command) where T : Command;

        Task RaiseEvent<T>(T Event) where T : Event;
    }
}
