﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Ioasys.Domain.Interfaces;

namespace Ioasys.Domain.Enterprises.Repository
{
    public interface IEnterpriseRepository : IRepository<Enterprise>
    {
        Task<List<Enterprise>> FilterEnterpriseAsync(string name, int typeId);
    }
}
