﻿using FluentValidation;
using Ioasys.Domain.Core.Models;
using System.Collections.Generic;

namespace Ioasys.Domain.Enterprises
{
    public class EnterpriseType : Entity<EnterpriseType>
    {

        #region constructors
        public EnterpriseType(){}
        #endregion

        public string Enterprise_Type_Name { get; set; }

        // EF Propriedade de Navegação
        public virtual ICollection<Enterprise> Enterprises { get; set; }

        #region Properties

        #endregion
        #region Methods
        public override bool IsValid()
        {
            Validate();
            return ValidationResult.IsValid;
        }

        public override void Validate()
        {
            ValidateName();
        }

        private void ValidateName()
        {
            RuleFor(c => c.Enterprise_Type_Name)
                .NotEmpty().WithMessage("O nome do tipo da Empresa precisa ser fornecido")
                .Length(2, 200).WithMessage("O nome do tipo da  Empresa precisa ter entre 2 e 150 caracteres");
        }

        
        #endregion
    }
}
