﻿using Ioasys.Application.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ioasys.Application.Interfaces
{
    public interface IEnterpriseAppService : IDisposable
    {
        Task Add(EnterpriseViewModel entity);
        Task<EnterpriseViewModel> GetById(int id);
        Task<List<EnterpriseViewModel>> GetAll();
        Task Remove(int id);
        Task<List<EnterpriseViewModel>> FilterEnterprise(string name, int typeId);
    }
}
