﻿using AutoMapper;
using Ioasys.Application.Interfaces;
using Ioasys.Application.ViewModels;
using Ioasys.Domain.Enterprises.Repository;
using Ioasys.Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Ioasys.Application.Services.Enterprise
{
    public class EnterpriseAppService : IEnterpriseAppService
    {
        private IEnterpriseRepository enterpriseRepository;
        private readonly IMapper mapper;
        private readonly IMediatorHandler mediator;

        public EnterpriseAppService(IEnterpriseRepository enterpriseRepository, IMapper mapper, IMediatorHandler mediator)
        {
            this.enterpriseRepository = enterpriseRepository;
            this.mapper = mapper;
            this.mediator = mediator;
        }

        public Task Add(EnterpriseViewModel entity)
        {
            throw new NotImplementedException();
        }

        public async Task<EnterpriseViewModel> GetById(int id)
        {
            return mapper.Map<EnterpriseViewModel>(await enterpriseRepository.GetById(id));
        }

        public async Task<List<EnterpriseViewModel>> GetAll()
        {
            return mapper.Map<List<EnterpriseViewModel>>(await enterpriseRepository.GetAll());
        }

        public async Task Remove(int id)
        {
            await enterpriseRepository.Remove(id);
        }

        public async Task<List<EnterpriseViewModel>> FilterEnterprise(string name, int typeId)
        {
            return mapper.Map<List<EnterpriseViewModel>>(await enterpriseRepository.FilterEnterpriseAsync(name, typeId));
        }

        public void Dispose()
        {
            enterpriseRepository.Dispose();
        }
    }
}
