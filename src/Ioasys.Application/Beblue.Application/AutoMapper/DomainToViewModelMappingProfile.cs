﻿using AutoMapper;
using Ioasys.Application.ViewModels;
using Ioasys.Domain.Enterprises;

namespace Ioasys.Application.AutoMapper
{
    class DomainToViewModelMappingProfile : Profile
    {
        public DomainToViewModelMappingProfile()
        {
            CreateMap<Enterprise, EnterpriseViewModel>();
            CreateMap<EnterpriseType, EnterpriseTypeViewModel>();
        }
    }
}
