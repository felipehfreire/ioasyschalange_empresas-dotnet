﻿using AutoMapper;
using Ioasys.Application.ViewModels;
using Ioasys.Domain.Enterprises;

namespace Ioasys.Application.AutoMapper
{
    public class ViewModelToDomainMappingProfile : Profile
    {
        public ViewModelToDomainMappingProfile()
        {
            CreateMap<EnterpriseViewModel, Enterprise>();
            CreateMap<Enterprise,EnterpriseViewModel>();

        }
    }
}
