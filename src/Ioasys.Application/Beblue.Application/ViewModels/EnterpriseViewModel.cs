﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Ioasys.Application.ViewModels
{
    public class EnterpriseViewModel
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "'Nome' é obrigatório")]
        [Display(Name = "Nome")]
        [MaxLength(200, ErrorMessage = "O tamanho máximo para 'Nome' é {1}")]
        public string Enterprise_Name { get;  set; }

        [Required(ErrorMessage = "'Descrição' é obrigatório")]
        [Display(Name = "Descrição")]
        [MaxLength(500, ErrorMessage = "O tamanho máximo para 'Descrição' é {1}")]
        public string Description { get; set; }

        [Display(Name = "Email")]
        public string Email_Enterprise { get;  set; }

        [Display(Name = "Facebook")]
        public string Facebook { get;  set; }

        [Display(Name = "Twitter")]
        public string Twitter { get;  set; }

        [Display(Name = "Linkedin")]
        public string Linkedin { get;  set; }

        [Display(Name = "TElefone")]
        public string Phone { get;  set; }

        [Display(Name = "Empresa Própria")]
        public bool Own_Enterprise { get;  set; }

        [Display(Name = "Foto")]
        public string Photo { get;  set; }

        [Display(Name = "valor")]
        public int Value { get;  set; }

        [Display(Name = "ações")]
        public int Shares { get;  set; }

        [Display(Name = "Preço das ações")]
        public int Share_Price { get;  set; }

        [Display(Name = "Ações próprias")]
        public int Own_Shares { get;  set; }

        [Display(Name = "Cidade")]
        public string City { get;  set; }

        [Display(Name = "Estado")]
        public string Country { get;  set; }

        [Display(Name = "enterprise_type")]
        public EnterpriseTypeViewModel Enterprise_Type { get; set; }
    }
}


