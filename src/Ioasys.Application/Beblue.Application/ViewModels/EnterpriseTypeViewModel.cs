﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Ioasys.Application.ViewModels
{
    public class EnterpriseTypeViewModel
    {
        [Key]
        public int Id { get; set; }

        [Required(ErrorMessage = "'Nome' é obrigatório")]
        [Display(Name = "Nome")]
        [MaxLength(200, ErrorMessage = "O tamanho máximo para 'Nome' é {1}")]
        public string Enterprise_Type_Name { get;  set; }

    }
}


