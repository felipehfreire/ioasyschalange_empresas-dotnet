﻿

using AutoMapper;
using Ioasys.Application.AutoMapper;
using Ioasys.Application.Interfaces;
using Ioasys.Application.Services.Enterprise;
using Ioasys.CrossCutting.Integrator;
using Ioasys.Data.Context;
using Ioasys.Data.Repositorys;
using Ioasys.Data.UOW;
using Ioasys.Domain.Core.Notifications;
using Ioasys.Domain.Enterprises.Repository;
using Ioasys.Domain.Interfaces;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace Ioasys.CrossCutting.IOC
{
    public static class IoasysIOC
    {
        public static void RegisterServices(this IServiceCollection services)
        {

            #region Application
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();
            services.AddSingleton<IConfigurationProvider>(AutoMapperConfiguration.RegisterMappings());
            services.AddScoped<IMapper>(sp => new Mapper(sp.GetRequiredService<IConfigurationProvider>(), sp.GetService));
            services.AddScoped<IEnterpriseAppService, EnterpriseAppService>();
            #endregion

            #region Domain

            services.AddScoped<INotificationHandler<DomainNotification>, DomainNotificationHandler>();
            services.AddScoped<IMediatorHandler, MediatorHandler>();
            #endregion

            #region data
            services.AddScoped<IUnityOfWork, UnitOfWork>();
            services.AddScoped<IoasysContext>();
            #endregion

            #region Repositories
            services.AddScoped<IEnterpriseRepository, EnterpriseRepository>();
            #endregion

          
        }
    }
}
